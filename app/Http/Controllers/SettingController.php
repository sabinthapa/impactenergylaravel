<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UpdateSettingValues;
use App\Services\Settings\SiteSetting;

class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $setting;

    public function __construct(SiteSetting $setting)
    {
        $this->middleware('auth');
        $this->setting = $setting;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $settings = $this->setting->all();
        return view('settings.index',compact('settings'));
    }

    public function update(UpdateSettingValues $request)
    {
        try {
            $settings = $this->setting->where($request->only('setting_title'))->update($request->only('setting_value'));
            return ['success'=>$settings,'message'=>'Settings updated'];
        } catch (Exception $e) {
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }

    public function getTemplate($type)
    {
        $settings = '';
        $templates = 'settings.templates.information';
        try{

            if($type == 'information'){
                $settings = $this->setting->whereIn('setting_title',['admin_email','admin_phone'])->get();
            }
            if($type == 'logo'){
                $settings = $this->setting->where('setting_title','admin_logo')->first();
                $templates = 'settings.templates.logo';
            }
            if($type == 'location'){
                $settings = $this->setting->whereIn('setting_title',['site_address','site_coords'])->get();   
            }
            if($type == 'social'){
                $settings = $this->setting->whereIn('setting_title',['site_facebook_link','site_google_link','site_pin_interest_link','site_viber_link','site_instagram_link','site_tweeter_link'])->get();
            }
            $view = view($templates,compact('settings'));
            return $view->render();
        } catch(Exception $error){
            return $error->getMessage();
        }
    }
}
