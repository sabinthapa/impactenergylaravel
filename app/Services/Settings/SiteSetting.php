<?php

namespace App\Services\Settings;

use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{

    protected $table = 'site_settings';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'setting_title', 'setting_value', 'display_text',
    ];

    protected $cast = [
    	'setting_value'=>'array'
    ];
}
