<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
        	'name'	=> 'Computalaya Networks',
        	'email'	=> 'admin@email.com',
        	'password'	=> bcrypt('pass_123'),
        ]);
    }
}
