<?php

use Illuminate\Database\Seeder;
use App\Services\Settings\SiteSetting;

class SiteConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach([
            [
                'setting_title'=>'admin_logo',
                'setting_value'=>'https://impactenergynepal.com/images/logo.png',
                'display_text'=>'Site Logo',
            ],
            [
                'setting_title'=>'admin_email',
                'setting_value'=>'impactenergy@mail.com',
                'display_text'=>'Admin Email',
            ],
            [
                'setting_title'=>'admin_phone',
                'setting_value'=>'01490001 | 9860810687',
                'display_text'=>'Admin Phone',
            ],
            [
                'setting_title'=>'site_address',
                'setting_value'=>'Mid Baneshwor, Kathmandu, Nepal',
                'display_text'=>'Address',
            ],
            [
                'setting_title'=>'site_coords',
                'setting_value'=>'1.4581, 25.12566',
                'display_text'=>'Co-ordinates',
            ],
            [
                'setting_title'=>'site_facebook_link',
                'setting_value'=>'https://www.facebook.com/fluxbomber',
                'display_text'=>'Facebook link',
            ],
            [
                'setting_title'=>'site_google_link',
                'setting_value'=>'https://myaccount.google.com/?utm_source=OGB&tab=rk&utm_medium=act',
                'display_text'=>'Google link',
            ],
            [
                'setting_title'=>'site_pin_interest_link',
                'setting_value'=>'https://myaccount.google.com/?utm_source=OGB&tab=rk&utm_medium=act',
                'display_text'=>'Pin Interest link',
            ],
            [
                'setting_title'=>'site_viber_link',
                'setting_value'=>'https://myaccount.google.com/?utm_source=OGB&tab=rk&utm_medium=act',
                'display_text'=>'Viber number',
            ],
            [
                'setting_title'=>'site_instagram_link',
                'setting_value'=>'https://myaccount.google.com/?utm_source=OGB&tab=rk&utm_medium=act',
                'display_text'=>'Instragram link',
            ],[
                'setting_title'=>'site_tweeter_link',
                'setting_value'=>'https://myaccount.google.com/?utm_source=OGB&tab=rk&utm_medium=act',
                'display_text'=>'Twitter link',
            ],
        ] as $setting){

            $save = SiteSetting::create($setting);
        }
    }
}
