@extends('layouts.admin')
@section('pageHeader','Settings')
@section('breadcrumb')
<a href="{{route('settings.index')}}">{{__('Settings')}}</a>
@endsection
@section('content')
<section class="content">
      <div class="row">
        <div class="col-md-3">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Setting</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body p-0">
              <ul class="nav nav-pills flex-column">
                <li class="nav-item changeLogo">
                  <a href="#" class="nav-link active">
                    <i class="fas fa-stamp"></i> Change Logo
                  </a>
                </li>

                <li class="nav-item changeInformation">
                  <a href="#" class="nav-link">
                    <i class="fas fa-info"></i> Information
                  </a>
                </li>

                <li class="nav-item changeLocation">
                  <a href="#" class="nav-link">
                    <i class="fas fa-map"></i> Locations & Maps
                  </a>
                </li>

                <li class="nav-item changeSocialLinks">
                  <a href="#" class="nav-link">
                    <i class="fas fa-link"></i> Socials Links
                  </a>
                </li>
              </ul>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9 settingsBody">
          <div class="card card-primary card-outline">
            <!-- /.card-header -->
            <div class="card-body p-0">
              
            </div>
            <div class="spinner-border setting-loader d-none" style="" role="status">
                <span class="sr-only">Loading...</span>
              </div>
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection

@push('scripts')
<script type="text/javascript">

  $(document).ready(function(){
    $('.changeLogo').trigger('click');
    $('[data-widget=pushmenu]').trigger('click');
  });
  $('.changeLogo').on('click',function(e){
    e.preventDefault();
    $(this).parent().find('a').removeClass('active')
    $(this).find('a').addClass('active')
    getSettingTemplate('{{route('settings.get_template','logo')}}');
  })
  $('.changeInformation').on('click',function(e){
    e.preventDefault();
    $(this).parent().find('a').removeClass('active')
    $(this).find('a').addClass('active')
    getSettingTemplate('{{route('settings.get_template','information')}}');
  })
  $('.changeSocialLinks').on('click',function(e){
    e.preventDefault();
    $(this).parent().find('a').removeClass('active')
    $(this).find('a').addClass('active')
    getSettingTemplate('{{route('settings.get_template','social')}}');
  })
  $('.changeLocation').on('click',function(e){
    e.preventDefault();
    $(this).parent().find('a').removeClass('active')
    $(this).find('a').addClass('active')
    getSettingTemplate('{{route('settings.get_template','location')}}');
  })

  let getSettingTemplate = (url = '') => {
    $('.setting-loader').removeClass('d-none')
    $.ajax({
      url:url,
      method:'get'
    }).
    then(result => {
      $('.setting-loader').addClass('d-none');
      $(document).find('.settingsBody').find('.card-body').html(result)  
    })
    .catch(error => {
      $('.setting-loader').addClass('d-none');
      console.info(error);
    })
  }
</script>
@endpush