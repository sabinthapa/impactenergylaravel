<table class="table">
        <thead>
          <tr>
            <th>{{ __('Setting') }}</th>
            <th>{{ __('Value') }}</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
         @foreach($settings as $setting)
         <tr>
           <td>{{$setting->display_text}}</td>
           <td>
              <span>{{$setting->setting_value}}</span>
              <div class="form-group d-none">
                 <input type="text" name="{{$setting->setting_title}}" class="form-control settingInput" placeholder="Enter" aria-describedby="{{$setting->setting_title}}-error" aria-invalid="false" value="{{$setting->setting_value}}">
                 <span id="{{$setting->setting_title}}-error" class="error invalid-feedback"></span>
               </div>
             </td>
             <td class="text-right py-0 align-middle">
               <div class="btn-group btn-group-sm">
                 <a href="#" class="btn btn-info editSetting"><i class="fas fa-edit"></i></a>
                 <a href="#" class="btn btn-danger d-none saveSetting">
                  <i class="fas fa-check"></i>
                  <div class="spinner-border d-none" style="width: 1rem !important; height: 1rem!important" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                </a>
              </div>
            </td>
          </tr> 
      @endforeach
    </tbody>
  </table>
<script>
  $(document).find('.editSetting').on('click',function(e){
   e.preventDefault();
   $(this).siblings('a.saveSetting').toggleClass('d-none');
   $(this).toggleClass('d-none');
   $(this).parents('tr').find('input').parent('.form-group').removeClass('d-none');
   $(this).parents('tr').find('span').addClass('d-none');
 });

  $(document).find('.saveSetting').on('click',function(e){
   e.preventDefault();
   let  settingEl = $(this), 
   formData = {
    setting_title:settingEl.parents('tr').find('input').attr('name'),
    setting_value:settingEl.parents('tr').find('input').val(),
    '_method':'PUT',
    '_token':'{{csrf_token()}}',
  };
  $(this).children().toggleClass('d-none');
  $.ajax({
    url:'{{route('settings.update')}}',
    method:'post',
    data:formData,
    dataType:'json',

  }).then(result => {

    if(result.success){
      settingEl.addClass('d-none');
      settingEl.siblings('a.editSetting').removeClass('d-none');

      settingEl.parents('tr').find('input').attr('aria-invalid','false').removeClass('is-invalid').parent('.form-group').toggleClass('d-none').find('span').addClass('d-none');
      settingEl.parents('tr').find('input').parent('.form-group').siblings().toggleClass('d-none').html(formData.setting_value);
    }
  }).catch(error => {
    let errors = error.responseJSON.message;
    settingEl.parents('tr')
    .find('input')
    .attr('aria-invalid',true)
    .addClass('is-invalid');
    let errorSpan = '#' +formData.setting_title + '-error';
    $(errorSpan).html(errors).removeClass('d-none');
  });
  settingEl.children().toggleClass('d-none');
});

</script>